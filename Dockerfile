FROM fedora:23
RUN yum -y install dnsmasq
EXPOSE 53
ADD run.sh /usr/bin/container-run
ADD regenerate-dnsmasq.py /usr/bin/regenerate-dnsmasq
CMD ["container-run"]
LABEL RUN "docker run -d -v /etc/containers/os-dnsmasq:/etc/containers/os-dnsmasq:Z ${NAME} ${IMAGE}"
