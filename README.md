ostack-dns-hack
---------------

Set up a nameserver that will return records for hosts known to the
OpenStack instance.  Otherwise, forward to the host's
`/etc/resolv.conf` servers.
